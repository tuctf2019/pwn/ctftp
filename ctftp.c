#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time
#include <sys/types.h>

// List of restricted files to not be downloaded
char *rfiles[1] = {
	"ctftp"
};

// Get to know your users
char username[32];

int menu() {
	printf("\nChoose a command:\n1) List Files\n2) Get Files\n");
	printf("> ");
	int mval = 0;
	scanf("%d", &mval);

	return mval;
}

// Gets the filesize of a file
// https://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}

// This is a better filter, but doesn't work for this CTF chal
/*
 * Old version used when using malloc()/heap
void filter(char **fileptr) {
	char *filt = malloc(64);
	memset(filt, 0, 64);
	char *filename = *fileptr;

	// remove: / \ , : ; `
	int len = strlen(filename);
	for (int i = 0, j = 0; i < len; i++) {
		char c = filename[i];
		if (c != '/' && c != '\\' && c != ',' && c != ':' && c != ';' && c != '`') {
			filt[j] = c; j++;
		}
	}

	free(filename); // Free previous heap addr
	*fileptr = filt;

}
*/

// Doesn't copy naughty characters
void filter(char *filename, int len) {
	char *filt = malloc(64);
	memset(filt, 0, 64);

	// remove: / \ , : ; `
	for (int i = 0, j = 0; i < len; i++) {
		char c = filename[i];
		if (c != '/' && c != '\\' && c != ',' && c != ':' && c != ';' && c != '`' && c != '\n') {
			filt[j] = c; j++;
		}
	}

	memcpy(filename, filt, 64);
	free(filt);
}

// the equivalent of .contains()
// Found somewhere on the internet
int match(char text[], char pattern[]) {
	int c, d, e, text_length, pattern_length, position = -1;
	text_length    = strlen(text);
	pattern_length = strlen(pattern);
	if (pattern_length > text_length)
		return -1;
	for (c = 0; c <= text_length - pattern_length; c++) {
		position = e = c;
		for (d = 0; d < pattern_length; d++) {
			if (pattern[d] == text[e])
				e++;
			else break;
		}
		if (d == pattern_length)
			return position;
	}
	return -1;
}

// Checks for restricted files
int restrictedFiles(char *filename) {
	int len = sizeof(rfiles)/sizeof(rfiles[0]);
	for (int i = 0; i < len; i++) {
		char *rfile = rfiles[i];
		int pos = match(filename, rfile);
		if (pos != -1) {
			printf("Nice try dirty hacker!\nYou're not getting '%s' anytime soon.\n", filename);
			return 1;
		}
	}
	return 0;
}

// Checks to make sure the file exists and is readable
int checkFile(char *filename) {
	// Does the file exist
	if( access(filename, F_OK) == -1 ) {
		printf("File not found: '%s'\n", filename);
		return 1;
	}
	// Can we read the file
	FILE *fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("Permissions denied: '%s'\n", filename);
		return 1;
	}

	return 0;
}

// Sends the file over the wire
// also: https://stackoverflow.com/questions/28197649/reading-binary-file-in-c-in-chunks
void sendFile(char *filename) {
	// Format when sending data
	// length:data

	FILE *file = NULL;
	unsigned char *buffer = malloc(1024);  // array of bytes, not pointers-to-bytes
	size_t bytesRead = 0;

	file = fopen(filename, "rb");

	if (file != NULL) {
		// Send the formatted length
		printf("%d:", fsize(file));
		// read up to sizeof(buffer) bytes
		while ((bytesRead = fread(buffer, 1, 1024, file)) > 0) {
			write(1, buffer, bytesRead);
		}
	}
	free(buffer);
}

// Request to download a file (contains buffer overflow)
void get() {
	// Get input
	printf("Enter filename: ");
	char filename[64];
	memset(filename, 0, 64);
	int len = read(0, filename, 128); // BOF

	// Prevent restricted files
	if (restrictedFiles(filename)) return;

	// CTF-style input filter
	filter(filename, len);

	// Checks the file exists, and we have permissions
	if (checkFile(filename)) return;
	
	printf("Sending File: %s\n", filename);
	sendFile(filename);
}

void welcome() {
	printf("Welcome to the CTFTP: the netcat-based FTP server\n");
	printf("Enter your name: ");
	scanf("%32s", username);
	printf("Welcome %s, feel free to browse my wares\n", username);
}


int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	welcome();

	while (1) {
		int m = menu();
		switch (m) {
			// ls
			case 1:
				system("ls"); break;
			// get
			case 2:
				get(); break;
			default:
				puts("Please enter a valid number"); break;
		}
	}

    return 0;
}
