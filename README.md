# ctftp

Desc: `Just what the world needs... another vulnerable FTP server. Have fun.`

Architecture: x86

Hints:

* Can you figure out the filter order?

Flag: `TUCTF{f1l73r_f1r57_7h3y_541d._y0u'll_b3_53cur3_7h3y_541d}`
